<?php

namespace App\Http\Controllers\Api\Computes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Conoha;

/**
 * Class ServersController.
 *
 * @package App\Http\Controllers\Api\Computes
 *
 * @property \Kyon2\Conoha\Api\Compute compute
 */
class ServersController extends Controller
{

    /**
     * ServersController constructor.
     */
    public function __construct()
    {
        $this->compute = Conoha::driver('Compute');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        $servers = $this->compute->getServers();
        return response()->json($servers);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($id)
    {
        $server = $this->compute->getServer($id);
        return response()->json($server);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
