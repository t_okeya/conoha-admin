<?php

namespace App\Http\Controllers\Api;

use App\Events\Auth\UserLoggedIn;
use App\Events\Auth\UserLoggedOut;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try
        {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials))
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        }
        catch (JWTException $e)
        {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        /** @var User $user */
        $user = Auth::createUserProvider('users')->retrieveByCredentials($credentials);
        event(new UserLoggedIn($user));

        // all good so return the token
        return response()->json(compact('user', 'token'));
    }

    public function logout()
    {
        $user = JWTAuth::parseToken()->authenticate();
        event(new UserLoggedOut($user));
    }

    public function getCurrentUser()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(compact('user'));
    }

    public function getUsers()
    {
        $users = User::all()->take(5);
        return response()->json(compact('users'));
    }
}
