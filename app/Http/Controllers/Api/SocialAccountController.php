<?php

namespace App\Http\Controllers\Api;

use App\Events\Auth\UserLoggedIn;
use App\Http\Controllers\Controller;
use App\Models\LinkedSocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Socialite;

class SocialAccountController extends Controller
{
    public function __construct()
    {
    }

    public function redirect($provider)
    {
        /** @var \Symfony\Component\HttpFoundation\RedirectResponse $redirectResponse */
        $redirectResponse = Socialite::driver($provider)->stateless()->redirect();

        \Log::info($redirectResponse->getTargetUrl());
        return response()->json([
            'redirect_url' => $redirectResponse->getTargetUrl()
        ]);
    }

    public function callback(Request $request, $provider)
    {
        ($request);
        try
        {
            $account = Socialite::with($provider)->stateless()->user();
        }
        catch (\Exception $e)
        {
            \Log::info($e->getMessage());
            return response()->json(['message' => "{$provider}での認証に失敗しました。"], 400);
        }

        $user = $this->createOrGetUser($account, $provider);
        $token = JWTAuth::fromUser($user);

        \Log::info($user);
        event(new UserLoggedIn($user));

        // all good so return the token
        return response()->json(compact('user', 'token'));
    }

    private function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        /** @var LinkedSocialAccount $account */
        $account = LinkedSocialAccount::firstOrNew([
            'provider_name' => $provider,
            'provider_id'   => $providerUser->getId()
        ]);

        $user = NULL;

        if ($account && $account->user)
        {
            $user = $account->user;
        }
        else
        {
            $user = User::firstOrNew([
                'email' => $providerUser->getEmail()
            ]);

            if (!$user)
            {
                $user = User::create([
                    'name'  => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                ]);
            }

            $user->accounts()->create([
                'user_id'       => $user->id,
                'provider_name' => $provider,
                'provider_id'   => $providerUser->getId()
            ]);
        }

        return $user;
    }
}
