<?php

namespace App\Http\Controllers\Api\BlockStorages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Conoha;

/**
 * Class TypesController.
 *
 * @package App\Http\Controllers\Api\BlockStorages
 *
 * @property \Kyon2\Conoha\Api\BlockStorage blockStorage
 */
class TypesController extends Controller
{

    /**
     * TypesController constructor.
     */
    public function __construct()
    {
        $this->blockStorage = Conoha::driver('BlockStorage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        $types = $this->blockStorage->getTypes();
        return response()->json($types);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($id)
    {
        $type = $this->blockStorage->getType($id);
        return response()->json($type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
