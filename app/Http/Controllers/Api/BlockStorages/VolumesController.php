<?php

namespace App\Http\Controllers\Api\BlockStorages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Conoha;

/**
 * Class VolumesController.
 *
 * @package App\Http\Controllers\Api\BlockStorages
 *
 * @property \Kyon2\Conoha\Api\BlockStorage blockStorage
 */
class VolumesController extends Controller
{

    /**
     * VolumesController constructor.
     */
    public function __construct()
    {
        $this->blockStorage = Conoha::driver('BlockStorage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        $volumes = $this->blockStorage->getVolumes();
        return response()->json($volumes);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($id)
    {
        $volume = $this->blockStorage->getVolume($id);
        return response()->json($volume);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
