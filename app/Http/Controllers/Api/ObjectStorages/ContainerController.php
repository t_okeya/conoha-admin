<?php

namespace App\Http\Controllers\Api\ObjectStorages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Conoha;

/**
 * Class ContainerController.
 *
 * @package App\Http\Controllers\Api\ObjectStorages
 *
 * @property \Kyon2\Conoha\Api\ObjectStorage objectStorage
 */
class ContainerController extends Controller
{

    /**
     * ContainerController constructor.
     */
    public function __construct()
    {
        $this->objectStorage = Conoha::driver('ObjectStorage');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $container
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($container)
    {
        $container = $this->objectStorage->getContainer($container);
        return response()->json($container);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $container
     */
    public function edit($container)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $container
     */
    public function update(Request $request, $container)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $container
     */
    public function destroy($container)
    {
        //
    }
}
