<?php

namespace App\Http\Controllers\Api\ObjectStorages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Conoha;

/**
 * Class ObjectController.
 *
 * @package App\Http\Controllers\Api\ObjectStorages
 *
 * @property \Kyon2\Conoha\Api\ObjectStorage objectStorage
 */
class ObjectController extends Controller
{

    protected $objectStorage;

    /**
     * ObjectController constructor.
     */
    public function __construct()
    {
        $this->objectStorage = Conoha::driver('ObjectStorage');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $container
     * @param $object
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($container, $object)
    {
        $object = $this->objectStorage->getObject($container, $object);
        return response()->json($object);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $container
     * @param $object
     */
    public function edit($container, $object)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $container
     * @param $object
     */
    public function update(Request $request, $container, $object)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $container
     * @param $object
     */
    public function destroy($container, $object)
    {
        //
    }
}
