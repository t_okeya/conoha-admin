<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Closure;

class QueryLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Config::get('app.debug') === false)
        {
            return $next($request);
        }

        $this->readyOutputQueryLog();

        $response = $next($request);

        $this->outputQueryLog();

        return $response;
    }

    /**
     * ready output query log.
     */
    private function readyOutputQueryLog()
    {
        foreach (Config::get('database.connections') as $db_name => $settings)
        {
            DB::connection($db_name)->enableQueryLog();
        }
    }

    /**
     * output query log.
     */
    private function outputQueryLog()
    {
        foreach (Config::get('database.connections') as $db_name => $settings)
        {
            $queries = DB::connection($db_name)->getQueryLog();
            if (empty($queries))
            {
                continue;
            }
            Log::debug('Query log, '. $db_name);
            foreach ($queries as $query)
            {
                Log::debug($query);
            }
        }
    }
}
