<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

/**
 * Class User.
 *
 * @package App\Models
 *
 * @property mixed id
 * @property mixed name
 * @property mixed email
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property LinkedSocialAccount[]|Collection|Relations\HasMany accounts
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return LinkedSocialAccount[]|Collection|Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }
}
