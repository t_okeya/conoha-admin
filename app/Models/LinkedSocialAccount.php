<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Support\Collection;

/**
 * Class LinkedSocialAccount.
 *
 * @package App\Models
 *
 * @property mixed id
 * @property mixed user_id
 * @property mixed provider_name
 * @property mixed provider_id
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property User|Collection|Relations\BelongsTo user
 */
class LinkedSocialAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider_name', 'provider_id'
    ];

    /**
     * @return User|Collection|Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
