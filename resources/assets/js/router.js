import VueRouter from 'vue-router'
import Vue from 'vue'

import UsersIndex from './components/users/index.vue'
import FlavorsIndex from './components/computes/flavors/index.vue'
import FlavorsDetail from './components/computes/flavors/detail.vue'
import ServersIndex from './components/computes/servers/index.vue'
import ServersDetail from './components/computes/servers/detail.vue'
import TypesIndex from './components/blockStorages/types/index.vue'
import TypesDetail from './components/blockStorages/types/detail.vue'
import VolumesIndex from './components/blockStorages/volumes/index.vue'
import VolumesDetail from './components/blockStorages/volumes/detail.vue'
import AuthLogin from './components/auths/login.vue'
import LoginRedirect from './components/auths/redirect.vue'
import LoginCallback from './components/auths/callback.vue'
import AboutsIndex from './components/abouts/index.vue'

Vue.use(VueRouter);

export default new VueRouter({

    mode: 'history',

    routes: [
        { path: '/', component: UsersIndex },
        { path: '/flavors', name: 'flavors-index', component: FlavorsIndex },
        { path: '/flavors/:id', name: 'flavors-detail', component: FlavorsDetail },
        { path: '/servers', name: 'servers-index', component: ServersIndex },
        { path: '/servers/:id', name: 'servers-detail', component: ServersDetail },
        { path: '/types', name: 'types-index', component: TypesIndex },
        { path: '/types/:id', name: 'types-detail', component: TypesDetail },
        { path: '/volumes', name: 'volumes-index', component: VolumesIndex },
        { path: '/volumes/:id', name: 'volumes-detail', component: VolumesDetail },
        { path: '/login', component: AuthLogin },
        { path: '/login/bitbucket', component: LoginRedirect },
        { path: '/login/bitbucket/callback', component: LoginCallback },
        { path: '/about', component: AboutsIndex }
    ],

    scrollBehavior (to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }

})
