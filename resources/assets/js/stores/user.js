import { http, ls } from '../services'

export const user = {

    debug: true,
    state: {
        users: [],
        user: {},
        authenticated: false,
    },

    login (email, password, successCb = null, errorCb = null) {
        const login_param = {email: email, password: password};
        http.post('login', login_param, res => {
            this.state.user = res.data.user;
            this.state.authenticated = true;
            successCb()
        }, error => {
            errorCb()
        })
    },

    callback (query, successCb = null, errorCb = null) {
        console.log(query);
        http.init();
        http.post('login/bitbucket/callback', query, res => {
            console.log(res.data.user);
            this.state.user = res.data.user;
            this.state.authenticated = true;
            successCb()
        }, error => {
            errorCb()
        })
    },

    getUsers () {
        http.get('users', res => {
            this.state.users = res.data.users
        })
    },

    // To log out, we just need to remove the token
    logout (successCb = null, errorCb = null) {
        http.get('logout', () => {
            ls.remove('jwt-token');
            this.state.authenticated = false;
            successCb()
        }, errorCb)
    },

    setCurrentUser () {
        const token = ls.get('jwt-token');
        if (token) {
            http.get('me', res => {
                this.state.user = res.data.user;
                this.state.authenticated = true;
            })
        }
    },

    /**
     * Init the service.
     */
    init () {
        this.setCurrentUser()
    }
}
