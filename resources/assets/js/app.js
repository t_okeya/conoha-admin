import Vue from 'vue'

import { http } from './services'
import router from './router'
import { user } from './stores'

import App from './app.vue'

require('./bootstrap')

const app = new Vue({
    router,
    render: h => h(App),
    created() {
        http.init()
        user.init()
    }
}).$mount('#app')
