<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    // normal auth
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout')->middleware('jwt.refresh');

    Route::get('users', 'AuthController@getUsers');

    // social auth
    Route::get('login/{provider}', 'SocialAccountController@redirect')->name('social.login');
    Route::post('login/{provider}/callback', 'SocialAccountController@callback')->name('social.callback');

    Route::group(['middleware' => 'jwt.auth'], function () {

        // me
        Route::get('me', 'AuthController@getCurrentUser');

        // BlockStorage
        Route::group(['namespace' => 'BlockStorages'], function () {
            Route::resource('types', 'TypesController', ['only' => ['index', 'show'], 'parameters' => ['types' => 'id']]);
            Route::resource('volumes', 'VolumesController', ['only' => ['index', 'show'], 'parameters' => ['volumes' => 'id']]);
        });

        // Computes
        Route::group(['namespace' => 'Computes'], function () {
            Route::resource('flavors', 'FlavorsController', ['only' => ['index', 'show'], 'parameters' => ['flavors' => 'id']]);
            Route::resource('servers', 'ServersController', ['only' => ['index', 'show'], 'parameters' => ['servers' => 'id']]);
        });

        // ObjectStorages
//        Route::group(['namespace' => 'ObjectStorages'], function () {
//            Route::resource('container', 'ContainerController', ['only' => ['show'], 'parameters' => ['container' => 'id']]);
//            Route::resource('object', 'ObjectController', ['only' => ['show'], 'parameters' => ['object' => 'id']]);
//        });
    });
});
