<?php

return [

    'tenant_id' => env('CONOHA_TENANT_ID'),
    'username'  => env('CONOHA_USERNAME'),
    'password'  => env('CONOHA_PASSWORD'),

];
